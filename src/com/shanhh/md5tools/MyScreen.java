package com.shanhh.md5tools;

import java.io.UnsupportedEncodingException;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.component.TextField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.util.StringProvider;

/**
 * A class extending the MainScreen class, which provides default standard
 * behavior for BlackBerry GUI applications.
 */
public final class MyScreen extends MainScreen {

    private static TextField textPrivateKey;
    private static TextField textPublicKey;

    private static LabelField lbRealMD5 = new LabelField();
    private static LabelField lbEncryptedMD5Full = new LabelField();
    private static LabelField lbEncryptedMD5Lite = new LabelField();

    private MenuItem menuAbout = new MenuItem(new StringProvider("About"), 0x00020000, 10) {
        public void run() {
            Dialog.inform("Author: Dan Shan\nyuer@shanhh.com");
        }
    };

    /**
     * Creates a new MyScreen object
     */
    public MyScreen() {
        // Set the displayed title of the screen
        setTitle("Simple MD5 Tools");

        textPrivateKey = new TextField(TextField.NO_LEARNING | TextField.NO_NEWLINE);
        textPrivateKey.setMaxSize(16);
        textPrivateKey.setLabel("Private key: ");

        textPublicKey = new TextField(TextField.NO_LEARNING | TextField.NO_NEWLINE);
        textPublicKey.setMaxSize(16);
        textPublicKey.setLabel("Public key: ");

        ButtonField btnMd5 = new ButtonField("Generate MD5");
        btnMd5.setChangeListener(new FieldChangeListener() {

            public void fieldChanged(Field field, int context) {
                String md5 = md5(textPrivateKey.getText());
                String encrytedMd5 = encyptMD5(textPrivateKey.getText(), textPublicKey.getText());

                lbRealMD5.setText(md5);
                lbEncryptedMD5Full.setText(encrytedMd5);
                lbEncryptedMD5Lite.setText(encrytedMd5.substring(0, 8));
            }
        });

        this.add(textPrivateKey);
        this.add(textPublicKey);

        this.add(btnMd5);
        this.add(new SeparatorField());

        this.add(new LabelField("Real MD5:"));
        this.add(lbRealMD5);
        this.add(new SeparatorField());

        this.add(new LabelField("Encrypted MD5 full:"));
        this.add(lbEncryptedMD5Full);
        this.add(new SeparatorField());

        this.add(new LabelField("Encrypted MD5 lite:"));
        this.add(lbEncryptedMD5Lite);
        
        this.addMenuItem(menuAbout);
        
        textPrivateKey.setFocus();
    }

    public boolean onClose() {
        super.close();
        return true;
    }

    protected boolean onSavePrompt() {
        return true;
    }

    private String md5(String source) {
        try {
            return MD5Digest.toMD5(source);
        } catch (UnsupportedEncodingException e) {
            return "Generate MD5 failed";
        }
    }

    private String encyptMD5(String privateKey, String publicKey) {
        String baseStr = privateKey + "@" + publicKey;
        baseStr = baseStr + baseStr.length();
        try {
            return MD5Digest.toMD5(baseStr);
        } catch (UnsupportedEncodingException e) {
            return "Generate MD5 failed";
        }
    }

}
